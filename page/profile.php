<!DOCTYPE html>
<html>
<head>
	<title>E-Voting Ketua Demustar Dan Wakil Ketua Demustar</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="../assets/css/style.css">
</head>
<body style="margin-top: 50px;">

	<?php  

	session_start();

	include '../assets/system/databaseInfo.php';

	if ($_SESSION['status'] != 'logged')
		echo '<script type="text/javascript">window.location.href = "../index.php?message=not_login"</script>';

	$nit = $_SESSION['nit'];

	$query = "SELECT * FROM users WHERE nit = '$nit'";

	$getData = mysqli_query($connection, $query);
	$result = mysqli_fetch_assoc($getData);

	?>

	<form class="form-signin" method="POST" action="../assets/system/fillProfile.php">
		<div class="text-center">
			<img src="../assets/img/Demustar_PPI.jpg" class="img-responsive" style="width: 50%;">  
			<img src="../assets/img/PPI_Madiun.png" class="img-responsive ml-3" style="width: 30%;">
		</div>
		
		<div class="text-center mt-2 mb-4">
			<h1 class="h3 mb-3 font-weight-normal">Silahkan isi terlebih dahulu</h1>
		</div>
	
		<?php

			if (isset($_GET['message'])) 
				if ($_GET['message'] == 'error')
					echo '<div class="alert alert-danger" role="alert">Terjadi kesalahan. Periksa koneksi jaringan anda!</div>';

			if ($_SESSION['nit'] == $nitAccepted) 	 
				echo '<div class="alert alert-info text-center" role="alert"><a href="dataPolling.php">Lihat data polling</a> || <a href="fillData.php">Tambah data login</a></div>';

		?>

		<?php if ($result['name'] == null || $result['study_program'] == null || $result['class_of_study_program'] == null) : ?>
			<div class="alert alert-warning" role="alert">Data hanya bisa diisi satu kali. Pastikan data yang anda inputkan benar</div>
		<?php else : ?>
			<div class="alert alert-warning" role="alert">Data tidak bisa diubah.</div>
		<?php endif; ?>

		<div class="form-label-group">
			<?php if ($result['name'] == null) : ?>
				<input type="text" id="inputName" name="nama" class="form-control" placeholder="Name" required autofocus>
			<?php else : ?>
				<input type="text" id="inputName" name="nama" class="form-control" placeholder="Name" value="<?= $result['name'] ?>" disabled>
			<?php endif; ?>
			<label for="inputName">Nama</label>
		</div>

		<div class="form-label-group">
			<?php if ($result['study_program'] == null) : ?>
				<input type="text" id="inputProdi" name="prodi" class="form-control" placeholder="Prodi" required>
			<?php else : ?>
				<input type="text" id="inputProdi" name="prodi" class="form-control" placeholder="Prodi" value="<?= $result['study_program']; ?>" disabled>
			<?php endif; ?>
			<label for="inputProdi">Prodi</label>
		</div>

		<div class="form-label-group">
			<?php if ($result['class_of_study_program'] == null) : ?>
				<input type="text" id="inputTingkat" name="tingkat" class="form-control" placeholder="Tingkat" required>
			<?php else : ?>
				<input type="text" id="inputTingkat" name="tingkat" class="form-control" placeholder="Jurusan" value="<?= $result['class_of_study_program']; ?>" disabled>
			<?php endif; ?>
			<label for="inputTingkat">Tingkat</label>
		</div>
		
		<?php if ($result['name'] == null || $result['study_program'] == null || $result['class_of_study_program'] == null) : ?>
			<button class="btn btn-lg btn-success btn-block" type="submit">Simpan Data</button>
		<?php else : ?>
			<a href="polling.php" class="btn btn-lg btn-info btn-block">Go to polling</a>
		<?php endif; ?>

		<p class="mt-5 mb-3 text-muted text-center">E-Voting Ketua Demustar Dan Wakil Ketua Demustar</p>
    </form>

	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

	<?php  

	mysqli_close($connection);

	?>
</body>
</html>