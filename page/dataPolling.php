<!DOCTYPE html>
<html>
<head>
	<title>E-Voting Ketua Demustar Dan Wakil Ketua Demustar</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>
<body style="padding: 30px; background-color: #f5f5f5;">

	<?php  

	session_start();

	include '../assets/system/databaseInfo.php';

	if ($_SESSION['nit'] != $nitAccepted) 
		echo '<script type="text/javascript">window.location.href = "../index.php?message=not_login"</script>';

	$query = "SELECT * FROM users";

	$data = [];

	for ($i = 1; $i < 7; $i++) { 
		$counter = 0;

		if ($i < 4) {
			$polledData = mysqli_query($connection, "SELECT * FROM users WHERE polled_leader = '$i'");
			while ($realData = mysqli_fetch_assoc($polledData)) {
				$counter++;
			}

			$data[$i-1] = $counter;
		
		} else {
			$polledData = mysqli_query($connection, "SELECT * FROM users WHERE polled_co_leader = '". ($i-3) ."'");
			while ($realData = mysqli_fetch_assoc($polledData)) {
				$counter++;
			}

			$data[$i-1] = $counter;
		}
	}

	$getData = mysqli_query($connection, $query);

			if (isset($_GET['message'])) 
				if ($_GET['message'] == 'error')
					echo '<div class="alert alert-danger w-50" role="alert">Terjadi kesalahan. Periksa koneksi jaringan anda!</div>';
				else if ($_GET['message'] == 'success') 
					echo '<div class="alert alert-success w-50" style="margin-left: 25%;" role="alert">Berhasil hapus data.</div>';


	?>

	<div class="w-50 float-left text-center">
		<h4>Hasil vote ketua</h4>
		<div id="ketua" style="width: 500px; height: 500px;"></div>
	</div>

	<div class="w-50 float-left text-center">
		<h4>Hasil vote wakil ketua</h4>
		<div id="wakil" style="width: 500px; height: 500px;"></div>
	</div>

	<div class="text-center mb-5">
		<h3 class="font-weight-normal">List Vote</h3>
		<a href="fillData.php" class="btn btn-lg btn-primary btn-block w-50" style="margin-left: 25%;" type="submit">Tambah Data</a>
	</div>

	<div>
		<table class="table table-striped">
		  <thead class="thead-dark">
		    <tr>
				<th scope="col">No</th>
				<th scope="col">NIT</th>
				<th scope="col">Nama</th>
				<th scope="col">Prodi</th>
				<th scope="col">Tingkat</th>
				<th scope="col">Pilihan Ketua</th>
				<th scope="col">Pilihan Wakil Ketua</th>
				<th scope="col">Aksi</th>
		    </tr>
		  </thead>
		  <tbody>
		  	<?php $no = 0; while($result = mysqli_fetch_assoc($getData)) {
		  		$no++;
		  	?>
		    <tr>
				<th scope="row"><?= $no; ?></th>
				<td><?= $result['nit']; ?></td>
				<td><?= $result['name']; ?></td>
				<td><?= $result['study_program']; ?></td>
				<td><?= $result['class_of_study_program']; ?></td>
				<td><?= $result['polled_leader']; ?></td>
				<td><?= $result['polled_co_leader']; ?></td>
				<td><a href="../assets/system/hapusData.php?nit=<?= $result['nit']; ?>" class="btn btn-danger btn-block">Hapus Data</a></td>
		    </tr>
			<?php } ?>
		  </tbody>
		</table>
	</div>

	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>

	<script type="text/javascript">
		var optionsKetua = {
		  	chart: {
		    	type: 'pie'
		  	},

		  	series: [<?= $data[0]; ?>, <?= $data[1]; ?>, <?= $data[2]; ?>],
		  	labels: ['M Kesuma C', 'M Nur Fadila', 'M Kusuma'],
		}

		var optionsWakil = {
		  	chart: {
		    	type: 'pie'
		  	},

		  	series: [<?= $data[3]; ?>, <?= $data[4]; ?>, <?= $data[5]; ?>],
		  	labels: ['Baskoro Adhi', 'Wahyuda Diru', 'Romdidi A'],
		}

		var chartKetua = new ApexCharts(document.querySelector("#ketua"), optionsKetua);
		var chartWakil = new ApexCharts(document.querySelector("#wakil"), optionsWakil);

		chartKetua.render();
		chartWakil.render();
	</script>

	<?php  

	mysqli_close($connection);

	?>

</body>
</html>