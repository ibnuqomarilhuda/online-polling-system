<!DOCTYPE html>
<html>
<head>
	<title>E-Voting Ketua Demustar Dan Wakil Ketua Demustar</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>
<body style="background-color: #f5f5f5;">

	<?php  

	session_start();

	include '../assets/system/databaseInfo.php';

	if ($_SESSION['status'] != 'logged')
		echo '<script type="text/javascript">window.location.href = "../index.php?message=not_login"</script>';

	$nit = $_SESSION['nit'];

	$query = "SELECT * FROM users WHERE nit = '$nit'";

	$getData = mysqli_query($connection, $query);
	$result = mysqli_fetch_assoc($getData);

	?>

	<form class="form-signin container mt-5" method="POST" action="../assets/system/fillPolling.php">
		<div class="text-center">
			<img src="../assets/img/Demustar_PPI.jpg" class="img-responsive" style="width: 17.5%;">  
			<img src="../assets/img/PPI_Madiun.png" class="img-responsive ml-3" style="width: 12.5%;">
		</div>
		
		<div class="text-center mb-4">
			<h1 class="h3 mb-3 font-weight-normal">Pilih yang anda inginkan</h1>
			
			<?php 

			if (isset($_GET['message'])) 
				if ($_GET['message'] == 'error')
					echo '<div class="alert alert-danger w-50" style="margin-left: 25%;" role="alert">Terjadi kesalahan. Periksa koneksi jaringan anda!</div>';
				else if ($_GET['message'] == 'success') 
					echo '<div class="alert alert-success w-50" style="margin-left: 25%;" role="alert">Pilihan anda disimpan!</div>';

			if ($_SESSION['nit'] == $nitAccepted) 
				echo '<div class="alert alert-info text-center w-50" style="margin-left: 25%;" role="alert"><a href="dataPolling.php">Lihat data polling</a> || <a href="fillData.php">Tambah data login</a></div>';

			?>

			<?php if ($result['polled_leader'] == null && $result['polled_co_leader'] == null) : ?>
				<div class="alert alert-warning w-50" style="margin-left: 25%;" role="alert">Data hanya bisa diisi satu kali. Pastikan data yang anda inputkan benar</div>
			<?php else : ?>
				<div class="alert alert-warning w-50" style="margin-left: 25%;" role="alert">Anda sudah memilih pilihan ketua dan wakil ketua. Silahkan logout!</div>
			<?php endif; ?>
		</div>

		<?php if ($result['polled_leader'] == null && $result['polled_co_leader'] == null) : ?>
			<div align="center">
				<label for="ketua1">
					<div>
						<img src="../assets/img/M_Kesuma_C.jpg" id="k1" class="m-3 img-responsive img-thumbnail" style="width: 150px;">
						<br />
						<input type="radio" id="ketua1" name="ketua" value="1">
						<p>M Kesuma C</p>
					</div>
				</label>
				<label for="ketua2">
					<div>
						<img src="../assets/img/M_Nur_Fadila.jpg" class="m-3 img-responsive img-thumbnail" style="width: 150px;">
						<br />
						<input type="radio" id="ketua2" name="ketua" value="2">
						<p>M Nur Fadila</p>
					</div>
				</label>
				<label for="ketua3">
					<div>
						<img src="../assets/img/M_Kusuma.jpg" class="m-3 img-responsive img-thumbnail" style="width: 150px;">
						<br />
						<input type="radio" id="ketua3" name="ketua" value="3">
						<p>M Kusuma</p>
					</div>
				</label>
			</div>

			<div align="center">
				<label for="wakil1">
					<div>
						<img src="../assets/img/Baskoro_Adhi.jpg" class="m-3 img-responsive img-thumbnail" style="width: 150px;">
						<br />
						<input type="radio" id="wakil1" name="wakil" value="1" style="display: ;">
						<p>Baskoro Adhi</p>
					</div>
				</label>
				<label for="wakil2">
					<div>
						<img src="../assets/img/Wahyuda_Diru.jpg" class="m-3 img-responsive img-thumbnail" style="width: 150px;">
						<br />
						<input type="radio" id="wakil2" name="wakil" value="2" style="display: ;">
						<p>Wahyuda Diru</p>
					</div>
				</label>
				<label for="wakil3">
					<div>
						<img src="../assets/img/Romdidi_A.jpg" class="m-3 img-responsive img-thumbnail" style="width: 150px;">
						<br />
						<input type="radio" id="wakil3" name="wakil" value="3" style="display: ;">
						<p>Romdidi A</p>
					</div>
				</label>
			</div>

			<button class="btn btn-lg btn-primary btn-block w-50" style="margin-left: 25%;" type="submit">Simpan Data</button>

		<?php else : ?>
			<a href="../assets/system/logout.php" class="btn btn-lg btn-secondary btn-block w-50" style="margin-left: 25%;">Logout</a>

		<?php endif; ?>

		<p class="mt-5 mb-3 text-muted text-center">E-Voting Ketua Demustar Dan Wakil Ketua Demustar</p>
	</form>

	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

	<?php  

	mysqli_close($connection);

	?>
</body>
</html>