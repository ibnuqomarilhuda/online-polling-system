<!DOCTYPE html>
<html>
<head>
	<title>E-Voting Ketua Demustar Dan Wakil Ketua Demustar</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="assets/css/style.css">
</head>
<body>
	<form class="form-signin" method="POST" action="assets/system/login.php">
		<div class="text-center">
			<img src="assets/img/Demustar_PPI.jpg" class="img-responsive" style="width: 50%;">  
			<img src="assets/img/PPI_Madiun.png" class="img-responsive ml-3" style="width: 30%;">
		</div>

		<div class="text-center mt-2 mb-4">
			<h1 class="h3 mb-3 font-weight-normal">Login</h1>
		</div>

		<?php 

		if (isset($_GET['message'])) {
			if ($_GET['message'] == 'error') 
				echo '<div class="alert alert-danger" role="alert">NIT atau password salah. Coba lagi!</div>';

			else if ($_GET['message'] == 'not_login') 
				echo '<div class="alert alert-danger" role="alert">Silahkan login dahulu!</div>';
		}

		?>

		<div class="form-label-group">
			<input type="text" id="inputEmail" name="nit" class="form-control" placeholder="NIT" required autofocus>
			<label for="inputEmail">NIT</label>
		</div>

		<div class="form-label-group">
			<input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" required>
			<label for="inputPassword">Password</label>
		</div>

		<button class="btn btn-lg btn-primary btn-block" type="submit">Login</button>
		<p class="mt-5 mb-3 text-muted text-center">E-Voting Ketua Demustar Dan Wakil Ketua Demustar</p>
    </form>

	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>
</html>